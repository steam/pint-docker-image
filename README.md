# Parallel in Time Docker Image

This repository is used to create a public Docker container with the requirements of the private parallel in time development repository. This Docker container can then be used to run parallel in time simulations on HTCondor.
