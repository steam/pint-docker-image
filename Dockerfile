# load cerngetdp image
FROM gitlab-registry.cern.ch/steam/cerngetdp:latest

# pint python requirements 
COPY requirements.txt .

# install requirements for fiqus and pint
RUN pip3 install -r requirements.txt 